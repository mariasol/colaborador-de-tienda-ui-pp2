package view;

import java.awt.Font;
import java.awt.SystemColor;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

@SuppressWarnings("serial")
public class EnterPurchasePanel extends JPanel {
	private JLabel productLabel;
	private JLabel amountLabel;
	private JLabel purchaseLabel;
	private JComboBox<String> productComboBox;
	private JButton addButton;
	private JButton cancelButton;
	private JSpinner spinner;
	private ProductPanel productPanel;
	
	public EnterPurchasePanel() {
		setBackground(SystemColor.scrollbar);
		setBounds(10, 52, 631, 537);
		setVisible(false);
		setLayout(null);
		
		initializeLabels();
		initializeComboBox();
		initializeButtons();
		initializeSpinner();
		initializePanels();
	}
	
	private void initializeLabels() {
		productLabel = new JLabel("Producto");
		productLabel.setFont(new Font("Segoe UI", Font.PLAIN, 17));
		productLabel.setBounds(36, 33, 100, 35);
		add(productLabel);
		
		amountLabel = new JLabel("Cantidad");
		amountLabel.setFont(new Font("Segoe UI", Font.PLAIN, 17));
		amountLabel.setBounds(331, 33, 85, 35);
		add(amountLabel);
	}
	
	private void initializeComboBox() {
		productComboBox = new JComboBox<String>();
		productComboBox.setFont(new Font("Segoe UI", Font.PLAIN, 17));
		productComboBox.setBounds(114, 35, 207, 31);
		add(productComboBox);
		
		hardcodeFill(productComboBox);		
	}
	
	private void initializeButtons() {
		addButton = new JButton("Agregar");
		addButton.setFont(new Font("Segoe UI", Font.PLAIN, 17));
		addButton.setBounds(479, 31, 111, 35);
		add(addButton);
		
		cancelButton = new JButton("Limpiar");
		cancelButton.setFont(new Font("Segoe UI", Font.PLAIN, 17));
		cancelButton.setBounds(479, 77, 111, 35);
		add(cancelButton);
		
		purchaseLabel = new JLabel("Compra");
		purchaseLabel.setFont(new Font("Segoe UI", Font.PLAIN, 17));
		purchaseLabel.setBounds(36, 136, 100, 35);
		add(purchaseLabel);
	}
	
	
	private void initializeSpinner() {
		spinner = new JSpinner();
		spinner.setFont(new Font("Segoe UI", Font.PLAIN, 17));
		spinner.setBounds(408, 35, 61, 31);
		
		SpinnerNumberModel spinnerModel = new SpinnerNumberModel();
        spinnerModel.setMinimum(0);
        spinner.setModel(spinnerModel);   
        
        add(spinner);
	}
	
	private void initializePanels() {
		productPanel = new ProductPanel();
		productPanel.setBounds(36, 173, 554, 45);
		add(productPanel);
		
		ProductPanel a = new ProductPanel();
		ProductPanel b = new ProductPanel();
		ProductPanel c = new ProductPanel();
		ProductPanel d = new ProductPanel();
		
		a.setBounds(36, 229, 554, 45); 
		b.setBounds(36, 285, 554, 45);
		c.setBounds(36, 341, 554, 45);
		d.setBounds(36, 397, 554, 45);
		add(a); add(b); add(c); add(d);
	}
	
	private void hardcodeFill(JComboBox<String> combo) {
		combo.addItem("Alfajor");
		combo.addItem("Agua");
		combo.addItem("Caramelo");
		combo.addItem("Chicle");
		combo.addItem("Chocolate");
		combo.addItem("Chupet�n");
		combo.addItem("Galletitas");
		combo.addItem("Gaseosa");
		combo.addItem("Helado");
		combo.addItem("Jugo");
		combo.addItem("Snack");
	}
}
