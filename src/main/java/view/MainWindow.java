package view;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class MainWindow {

	private JFrame mainFrame;
	private SummaryProductsPanel summaryProductsPanel;
	private EnterPurchasePanel enterPurchasePanel;
	private PredictionPanel predictionPanel;
	private ProfilePanel profilePanel;
	
	//inicializaci�n pesta�as
	private JLabel summaryProductsLabel;
	private JLabel enterPurchaseLabel;
	private JLabel predictionLabel;
	private JLabel profileLabel;
	
/* TODO: eliminar este comentario | usarlo como referencia para armar el main principal
	//Launch the application.
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.mainFrame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
*/
	public MainWindow() {
		initialize();

		initializePanels();
		initializeLabels();
	}

	private void initialize() {
		mainFrame = new JFrame();
		mainFrame.getContentPane().setFont(new Font("Segoe UI", Font.PLAIN, 11));
		mainFrame.setTitle("Colaborador de Tienda");
		mainFrame.setBounds(100, 100, 666, 640);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setLocationRelativeTo(null);
		mainFrame.setResizable(false);
		mainFrame.getContentPane().setLayout(null);
				
	}

	private void initializePanels() {
		enterPurchasePanel = new EnterPurchasePanel();
		summaryProductsPanel = new SummaryProductsPanel();
		predictionPanel = new PredictionPanel();
		profilePanel = new ProfilePanel();
		mainFrame.getContentPane().add(enterPurchasePanel);
		mainFrame.getContentPane().add(summaryProductsPanel);
		mainFrame.getContentPane().add(predictionPanel);
		mainFrame.getContentPane().add(profilePanel);
	}
	
	private void initializeLabels() {
		summaryProductsLabel = new JLabel("Res�men de Productos");
		summaryProductsLabel.setBounds(10, 11, 219, 41);
		mainFrame.getContentPane().add(summaryProductsLabel);
		setStyleLabel(summaryProductsLabel);
		showPanel(summaryProductsLabel, summaryProductsPanel);		
		
		enterPurchaseLabel = new JLabel("Ingresar Compra");
		enterPurchaseLabel.setBounds(239, 11, 167, 41);
		mainFrame.getContentPane().add(enterPurchaseLabel);
		setStyleLabel(enterPurchaseLabel);
		showPanel(enterPurchaseLabel, enterPurchasePanel);		
		
		predictionLabel = new JLabel("Predicciones");
		predictionLabel.setBounds(416, 11, 138, 41);
		mainFrame.getContentPane().add(predictionLabel);
		setStyleLabel(predictionLabel);
		showPanel(predictionLabel, predictionPanel);		
		
		profileLabel = new JLabel("Perfil");
		profileLabel.setBounds(564, 11, 77, 41);
		mainFrame.getContentPane().add(profileLabel);
		setStyleLabel(profileLabel);
		showPanel(profileLabel, profilePanel);
	}
	
	private void setStyleLabel(JLabel label) {
		label.setOpaque(true);
		label.setBackground(SystemColor.controlHighlight);
		label.setFont(new Font("Segoe UI", Font.PLAIN, 19));
		label.setHorizontalAlignment(SwingConstants.CENTER);
	}
	
	private void showPanel(JLabel label, JPanel panel) {
		label.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				label.setBackground(SystemColor.scrollbar);				
			}
			@Override
			public void mouseExited(MouseEvent e) {
					if(!panel.isVisible())
						label.setBackground(SystemColor.controlHighlight);
	        }
			@Override
			public void mouseClicked(MouseEvent e) {

				switch (label.getText()){
				case "Ingresar Compra":
					selectLabel(label);
					panel.setVisible(true);
					unselectLabel(summaryProductsLabel);
					unselectLabel(predictionLabel);
					unselectLabel(profileLabel);
					hidePanel(summaryProductsPanel);
					hidePanel(predictionPanel);
					hidePanel(profilePanel);
					break;
					
				case "Res�men de Productos":
					selectLabel(label);
					panel.setVisible(true);
					unselectLabel(enterPurchaseLabel);
					unselectLabel(predictionLabel);
					unselectLabel(profileLabel);
					hidePanel(enterPurchasePanel);
					hidePanel(predictionPanel);
					hidePanel(profilePanel);
					break;
					
				case "Predicciones":
					selectLabel(label);
					panel.setVisible(true);
					unselectLabel(summaryProductsLabel);
					unselectLabel(enterPurchaseLabel);
					unselectLabel(profileLabel);
					hidePanel(summaryProductsPanel);
					hidePanel(enterPurchasePanel);
					hidePanel(profilePanel);
					break;
					
				case "Perfil":
					selectLabel(label);
					panel.setVisible(true);
					unselectLabel(summaryProductsLabel);
					unselectLabel(enterPurchaseLabel);
					unselectLabel(predictionLabel);
					hidePanel(summaryProductsPanel);
					hidePanel(enterPurchasePanel);
					hidePanel(predictionPanel);
					break;
					
				default:
					System.out.println("NADA");
				}
			}
		});
	}
	
	public SummaryProductsPanel getSummaryProductsPanel() {
		return summaryProductsPanel;
	}

	public void setSummaryProductsPanel(SummaryProductsPanel summaryProductsPanel) {
		this.summaryProductsPanel = summaryProductsPanel;
	}

	public EnterPurchasePanel getEnterPurchasePanel() {
		return enterPurchasePanel;
	}

	public void setEnterPurchasePanel(EnterPurchasePanel enterPurchasePanel) {
		this.enterPurchasePanel = enterPurchasePanel;
	}

	public PredictionPanel getPredictionPanel() {
		return predictionPanel;
	}

	public void setPredictionPanel(PredictionPanel predictionPanel) {
		this.predictionPanel = predictionPanel;
	}

	public ProfilePanel getProfilePanel() {
		return profilePanel;
	}

	public void setProfilePanel(ProfilePanel profilePanel) {
		this.profilePanel = profilePanel;
	}

	public void selectLabel(JLabel label) {
		label.setBackground(SystemColor.scrollbar);		
		label.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 19));
		label.setForeground(new Color(0, 0, 128));
	}
	
	public void unselectLabel(JLabel label) {
		label.setBackground(SystemColor.controlHighlight);
		label.setFont(new Font("Segoe UI", Font.PLAIN, 19));
		label.setForeground(new Color(0, 0, 0));
	}
	
	public void hidePanel(JPanel panel) {
		panel.setVisible(false);
	}

	public JLabel getSummaryProductsLabel() {
		return summaryProductsLabel;
	}

	public void setSummaryProductsLabel(JLabel summaryProductsLabel) {
		this.summaryProductsLabel = summaryProductsLabel;
	}

	public JLabel getEnterPurchaseLabel() {
		return enterPurchaseLabel;
	}

	public void setEnterPurchaseLabel(JLabel enterPurchaseLabel) {
		this.enterPurchaseLabel = enterPurchaseLabel;
	}

	public JFrame getMainFrame() {
		return mainFrame;
	}

	public JLabel getPredictionLabel() {
		return predictionLabel;
	}

	public void setPredictionLabel(JLabel predictionLabel) {
		this.predictionLabel = predictionLabel;
	}

	public JLabel getProfileLabel() {
		return profileLabel;
	}

	public void setProfileLabel(JLabel profileLabel) {
		this.profileLabel = profileLabel;
	}
	
	
}
