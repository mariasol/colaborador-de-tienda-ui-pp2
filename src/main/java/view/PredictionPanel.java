package view;

import java.awt.Font;
import java.awt.SystemColor;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumnModel;

@SuppressWarnings("serial")
public class PredictionPanel extends JPanel {

	private JTable predictionTable;

	public PredictionPanel() {
		setBackground(SystemColor.scrollbar);
		setBounds(10, 52, 631, 537);
		setVisible(false);
		setLayout(null);
		
		initializeTable();
	}
	
	private void initializeTable() {		
	    predictionTable = new JTable(new Object[50][3], new Object [] {"Producto", "Cantidad","Fecha"});
	    setStyleTable(predictionTable);
        setWidth(predictionTable, new int [] {300,129,129});

	    JScrollPane tableScroll = new JScrollPane();
	    tableScroll.setViewportView(predictionTable);
        tableScroll.setBounds(35, 67, 558, 443);
        add(tableScroll);        
	}
	
	private void setStyleTable(JTable table) {
		JTableHeader tableHeader = table.getTableHeader();
        tableHeader.setFont(new Font("Segoe UI", Font.PLAIN, 15));
        table.setRowHeight(25);
	}
	
	private void setWidth(JTable table, int[] widths) {
		//faltan validaciones
		TableColumnModel columnModel = table.getColumnModel();
		for(int i=0; i<widths.length; i++) {
			columnModel.getColumn(i).setPreferredWidth(widths[i]);
		}
	}
}
