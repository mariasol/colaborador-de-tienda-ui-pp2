package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class ProfilePanel extends JPanel {

	private JLabel emailLabel;
	private JLabel notificationLabel;
	private JRadioButton noButton;
	private JRadioButton yesButton;
	private ButtonGroup buttonGroup;
	private JTextField emailField;

	public ProfilePanel() {
		setBackground(SystemColor.scrollbar);
		setBounds(10, 52, 631, 537);
		setVisible(false);
		setLayout(null);
		
		initializeLabels();
		initializeRadioButton();
		initializeField();
	}
	
	private void initializeLabels() {
		emailLabel = new JLabel("Email:");
		emailLabel.setFont(new Font("Segoe UI", Font.PLAIN, 17));
		emailLabel.setBounds(36, 79, 55, 35);
		add(emailLabel);
		
		notificationLabel = new JLabel("Deseo recibir notificaciones");
		notificationLabel.setFont(new Font("Segoe UI", Font.PLAIN, 17));
		notificationLabel.setBounds(36, 33, 225, 35);
		add(notificationLabel);
	}
	
	private void initializeRadioButton() {
		yesButton = new JRadioButton("S\u00ED");
		yesButton.setBackground(SystemColor.scrollbar);
		yesButton.setFont(new Font("Segoe UI", Font.PLAIN, 17));
		yesButton.setBounds(272, 33, 55, 39);
		yesButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				checkRadioButtons();
			}
		});
		add(yesButton);
		
		noButton = new JRadioButton("No");
		noButton.setFont(new Font("Segoe UI", Font.PLAIN, 17));
		noButton.setBackground(SystemColor.scrollbar);
		noButton.setBounds(329, 33, 55, 39);
		noButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				checkRadioButtons();
			}
		});
		add(noButton);
		
		buttonGroup = new ButtonGroup();
		buttonGroup.add(yesButton);
		buttonGroup.add(noButton);
	}
	
	private void initializeField() {
		emailField = new JTextField();		
		emailField.setFont(new Font("Segoe UI", Font.PLAIN, 17));
		emailField.setBounds(89, 79, 326, 35);
		add(emailField);
		emailField.setColumns(10);
	}
	
	private void checkRadioButtons() {
		if(yesButton.isSelected()) {
			emailField.setEnabled(true);
			emailLabel.setForeground(new Color(0,0,0));
		}
		if(noButton.isSelected()) {
			emailField.setEnabled(false);
			emailLabel.setForeground(SystemColor.textInactiveText);
		}
	}
}
