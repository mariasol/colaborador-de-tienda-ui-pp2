package view;

import java.awt.Font;
import java.awt.SystemColor;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumnModel;

@SuppressWarnings("serial")
public class SummaryProductsPanel extends JPanel {

	private JTable stockTable;
	private JTable missingStockTable;
	private JLabel productsLabel;
	private JLabel missingStockLabel;
	private DefaultTableModel tableModel; 

	public SummaryProductsPanel() {
		setBackground(SystemColor.scrollbar);
		setBounds(10, 52, 631, 537);
		setVisible(false);
		setLayout(null);
		
		initializeLabels();
		initializeTables();
	}
	
	public JTable getStockTable() {
		return stockTable;
	}

	

	public JTable getMissingStockTable() {
		return missingStockTable;
	}

	public void setMissingStockTable(JTable missingStockTable) {
		this.missingStockTable = missingStockTable;
	}

	private void initializeLabels() {
		productsLabel = new JLabel("Stock actual");
		productsLabel.setFont(new Font("Segoe UI", Font.PLAIN, 17));
		productsLabel.setBounds(36, 33, 100, 35);
		add(productsLabel);
		
        missingStockLabel = new JLabel("Stock faltante");
        missingStockLabel.setFont(new Font("Segoe UI", Font.PLAIN, 17));
        missingStockLabel.setBounds(325, 33, 189, 35);
        add(missingStockLabel);

	}
	
	private void initializeTables() {		
		tableModel=  new DefaultTableModel();
		
		tableModel.addColumn("Producto");
		tableModel.addColumn("Cantidad");
		stockTable = new JTable(tableModel);
	    //stockTable = new JTable(new Object[50][2], new Object [] {"Producto", "Cantidad"});
		
	    
	    setStyleTable(stockTable);
        setWidth(stockTable, new int [] {167,100});
        
	    JScrollPane stockScroll = new JScrollPane();
	    stockScroll.setViewportView(stockTable);
        stockScroll.setBounds(35, 67, 267, 443);
        add(stockScroll);
        
        
	    missingStockTable = new JTable(new Object[50][1], new Object [] {"Producto"});
	    setStyleTable(missingStockTable);
	    add(missingStockLabel);
	    JScrollPane missingStockScroll = new JScrollPane();
	    missingStockScroll.setViewportView(missingStockTable);
        missingStockScroll.setBounds(325, 67, 267, 443);
        add(missingStockScroll);

        
	}
	
	private void setStyleTable(JTable table) {
		JTableHeader tableHeader = table.getTableHeader();
        tableHeader.setFont(new Font("Segoe UI", Font.PLAIN, 15));
        table.setRowHeight(25);
	}
	
	private void setWidth(JTable table, int[] widths) {
		//faltan validaciones
		TableColumnModel columnModel = table.getColumnModel();
		for(int i=0; i<widths.length; i++) {
			columnModel.getColumn(i).setPreferredWidth(widths[i]);
		}
	}

	public DefaultTableModel getTableModel() {
		return tableModel;
	}

	public void setTableModel(DefaultTableModel tableModel) {
		this.tableModel = tableModel;
	}

	public void setStockTable(JTable stockTable) {
		this.stockTable = stockTable;
	}
}
