package view;

import java.awt.Font;
import java.awt.SystemColor;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

@SuppressWarnings("serial")
public class ProductPanel extends JPanel {

	public ProductPanel() {
		setForeground(SystemColor.control);
		setLayout(null);

		initializeLabels();
		initializeButtons();
	}

	private void initializeLabels() {
		JLabel nameProductLabel = new JLabel("nombre_producto");
		nameProductLabel.setFont(new Font("Segoe UI", Font.PLAIN, 17));
		nameProductLabel.setBounds(10, 11, 273, 23);
		add(nameProductLabel);
		
		JLabel amountLabel = new JLabel("n");
		amountLabel.setHorizontalAlignment(SwingConstants.CENTER);
		amountLabel.setFont(new Font("Segoe UI", Font.PLAIN, 17));
		amountLabel.setBounds(377, 11, 53, 23);
		add(amountLabel);
	}
	
	private void initializeButtons() {
		JButton subtractButton = new JButton("-");
		subtractButton.setFont(new Font("Segoe UI", Font.PLAIN, 16));
		subtractButton.setBounds(329, 11, 45, 23);
		add(subtractButton);
		
		JButton addButton = new JButton("+");
		addButton.setFont(new Font("Segoe UI", Font.PLAIN, 16));
		addButton.setBounds(432, 11, 45, 23);
		add(addButton);
		
		JButton deleteButton = new JButton("x");
		deleteButton.setFont(new Font("Segoe UI", Font.PLAIN, 16));
		deleteButton.setBounds(485, 11, 45, 23);
		add(deleteButton);
	}
}
