package colaborador.de.tienda.ui.pp2;

import java.io.File;

import controller.MainWindow_Controller;
import model.ColaboradorDeTienda;
import model.dao.implementation.DAOJSONFactory;
import view.MainWindow;

public class Main {

	public static void main(String[] args) {
		System.out.println("arg: <"+args[0]+">");
		String pathJsonFile="";
		boolean noSeEncontroFile=true;
		if(!args[0].isEmpty()) { //para pasar el path
			if(new File(args[0]).isFile()) { //chequeo que el path sea un file
				pathJsonFile=args[0];
				noSeEncontroFile=false;
			}
		}
		if(noSeEncontroFile) { //Uso uno hardcodeado si no se puede usar el argumento
			pathJsonFile=new File("").getAbsolutePath()+"/src/main/java/colaborador/de/tienda/ui/pp2/base.json";
		}
		System.out.println("path es "+pathJsonFile);
		ColaboradorDeTienda colaborador= new ColaboradorDeTienda(new DAOJSONFactory(pathJsonFile));
		MainWindow view = new MainWindow();
		MainWindow_Controller mainWindowController=new MainWindow_Controller(view, colaborador);
		mainWindowController.init();
	}

}
