package controller;

import java.awt.SystemColor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

import model.ColaboradorDeTienda;
import model.dto.Product_DTO;
import view.MainWindow;

public class MainWindow_Controller{

	private MainWindow view;
	private ColaboradorDeTienda model;
	
	/**
	 * @wbp.parser.entryPoint
	 */
	public MainWindow_Controller(MainWindow view, ColaboradorDeTienda model) {
		
		this.view = view;
		this.model = model;
	}
	
	public void init() {
		fillProductTableSummaryProductPanel(); 
		showPanel(view.getEnterPurchaseLabel() , view.getEnterPurchasePanel()); //para mostrar panel al clickear en ellos
		view.getMainFrame().setVisible(true);
	}
	private void fillProductTableSummaryProductPanel() {
		DefaultTableModel tableModel = view.getSummaryProductsPanel().getTableModel();
		List<Product_DTO> products = model.readAllProducts();
		fillTableModelProducts(tableModel,products);
	}

	private void fillTableModelProducts(DefaultTableModel tableModel, List<Product_DTO> list) {
		for(int i=0;i<list.size();i++) {
			
			tableModel.addRow(new Object []{list.get(i).getName(), "2"});
		}
		
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	private void showPanel(JLabel label, JPanel panel) {
		label.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				label.setBackground(SystemColor.scrollbar);				
			}
			@Override
			public void mouseExited(MouseEvent e) {
					if(!panel.isVisible())
						label.setBackground(SystemColor.controlHighlight);
	        }
			@Override
			public void mouseClicked(MouseEvent e) {

				switch (label.getText()){
				case "Ingresar Compra":
					view.selectLabel(label);
					panel.setVisible(true);
					view.unselectLabel(view.getSummaryProductsLabel());
					view.unselectLabel(view.getPredictionLabel());
					view.unselectLabel(view.getProfileLabel());
					view.hidePanel(view.getSummaryProductsPanel());
					view.hidePanel(view.getPredictionPanel());
					view.hidePanel(view.getProfilePanel());
					break;
				
					/* 
				case "Res�men de Productos":
					selectLabel(label);
					panel.setVisible(true);
					unselectLabel(enterPurchaseLabel);
					unselectLabel(predictionLabel);
					unselectLabel(profileLabel);
					hidePanel(enterPurchasePanel);
					hidePanel(predictionPanel);
					hidePanel(profilePanel);
					break;
					
				case "Predicciones":
					selectLabel(label);
					panel.setVisible(true);
					unselectLabel(summaryProductsLabel);
					unselectLabel(enterPurchaseLabel);
					unselectLabel(profileLabel);
					hidePanel(summaryProductsPanel);
					hidePanel(enterPurchasePanel);
					hidePanel(profilePanel);
					break;
					
				case "Perfil":
					selectLabel(label);
					panel.setVisible(true);
					unselectLabel(summaryProductsLabel);
					unselectLabel(enterPurchaseLabel);
					unselectLabel(predictionLabel);
					hidePanel(summaryProductsPanel);
					hidePanel(enterPurchasePanel);
					hidePanel(predictionPanel);
					break;
					
				default:
					System.out.println("NADA");*/
				}
			}
		});
	}
}
